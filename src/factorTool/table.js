const pieceTable = (pieceTableId) => {
  var table = document.getElementById(pieceTableId);
  table.innerHTML = '';
  var thead = table.createTHead();
  var row = thead.insertRow();
  var headers = [
    '<b>---</b>',
    '<b>a</b>',
    '<b>b</b>',
    '<b>c</b>',
    '<b>d</b>',
    '<b>e</b>',
    '<b>f</b>',
    '<b>g</b>',
    '<b>h</b>'
  ];
  for(var i = 0;i < headers.length;i++) {
    row.insertCell().innerHTML = headers[i];
  }
  var rows = [
    '8',
    '7',
    '6',
    '5',
    '4',
    '3',
    '2',
    '1'
  ];
  for(var i = 0;i < rows.length;i++) {
    row = table.insertRow();
    row.insertCell().innerHTML = '<b>' + rows[i] + '</b>';
    for(var j = 0;j < 8;j++) {
      cell = row.insertCell();
      if(i % 2 !== j % 2) {
        cell.innerHTML = `<input id='${pieceTableId}_${['a','b','c','d','e','f','g','h'][j]}${rows[i]}' style='width: 30px; height: 30px; background-color: #bbbbbb;' type='text'/>`;
      }
      else {
        cell.innerHTML = `<input id='${pieceTableId}_${['a','b','c','d','e','f','g','h'][j]}${rows[i]}' style='width: 30px; height: 30px' type='text'/>`;
      }
    }
  }
}

pieceTable('table_P');
pieceTable('table_B');
pieceTable('table_N');
pieceTable('table_R');
pieceTable('table_S');
pieceTable('table_Q');
pieceTable('table_K');