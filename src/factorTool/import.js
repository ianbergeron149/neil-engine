const importFunc = () => {
  var obj = {};
  try {
    obj = JSON.parse(document.getElementById(`import`).value);
  }
  catch(err) {}

  document.getElementById(`checkmate`).value = '';
  if(obj['checkmate']) {
    document.getElementById(`checkmate`).value = obj['checkmate'];
  }
  document.getElementById(`check`).value = '';
  if(obj['check']) {
    document.getElementById(`check`).value = obj['check'];
  }
  document.getElementById(`timelineAdvantage`).value = '';
  if(obj['timelineAdvantage']) {
    document.getElementById(`timelineAdvantage`).value = obj['timelineAdvantage'];
  }
  document.getElementById(`inactiveTimelines`).value = '';
  if(obj['inactiveTimelines']) {
    document.getElementById(`inactiveTimelines`).value = obj['inactiveTimelines'];
  }
  document.getElementById(`spatialMove`).value = '';
  if(obj['spatialMove']) {
    document.getElementById(`spatialMove`).value = obj['spatialMove'];
  }
  ['P','B','N','R','S','Q','K'].forEach((piece) => {
    document.getElementById(`base_${piece}`).value = '';
    if(obj[`${piece}`]) {
      document.getElementById(`base_${piece}`).value = obj[`${piece}`];
    }
    ['a','b','c','d','e','f','g', 'h'].forEach((file) => {
      ['1','2','3','4','5','6','7','8'].forEach((rank) => {
        document.getElementById(`table_${piece}_${file}${rank}`).value = '';
        if(obj[`${piece}${file}${rank}`]) {
          document.getElementById(`table_${piece}_${file}${rank}`).value = obj[`${piece}${file}${rank}`];
        }
      });
    });
  });
}

document.getElementById('import_button').onclick = importFunc;